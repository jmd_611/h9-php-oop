<?php

require_once "Frog.php";
require_once "Ape.php";


$sheep = new Animal("Shaun");
echo "Name : ".$sheep->get_name(); // "shaun"
echo "<br/> Legs : ".$sheep->get_legs(); // 4
echo "<br/> Cold blooded : ".$sheep->get_cold_blooded(); // "no"


$kodok = new Frog("Buduk");
$kodok->getFrogName();
$kodok->getFrogLegs();
$kodok->getFrogBlood();
$kodok->getFrogJump();

$ape = new Ape("kera sakti");
$ape->getApeName();
$ape->getApeLegs();
$ape->getApeBlood();
$ape->getApeYell();

?>