<?php
require_once "Animal.php";

class Ape extends Animal
{
    public $legs = 2;
    public function getApeName()
    {
        echo "<br/><br/>Name : ".$this->get_name();
    }
    public function getApeLegs()
    {
        echo "<br/>Legs : ".$this->legs;
    }
    public function getApeBlood()
    {
        echo "<br/>Cold blooded : ".$this->get_cold_blooded();
    }
    public function getApeYell()
    {
        echo "<br/>Yell : Auoooo..";
    }
}

?>