<?php
require_once "Animal.php";


class Frog extends Animal
{
    public function getFrogName()
    {
        echo "<br/><br/>Name : ".$this->get_name();
    }
    
    public function getFrogLegs()
    {
        echo "<br/>Legs : ".$this->get_legs();
    }
    public function getFrogBlood()
    {
        echo "<br/>Cold blooded : ".$this->get_cold_blooded();
    }
    public function getFrogJump()
    {
        echo "<br/>Jump : Hop Hop";
    }
}
?>